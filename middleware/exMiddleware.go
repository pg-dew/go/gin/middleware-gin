package middleware

import (
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
)

type Result struct {
	Name string `json:"name"`
}

func ExampleMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		var input Result
		e := c.ShouldBindJSON(&input)
		if e != nil {
			fmt.Println(e)
		}
		if input.Name == "" {
			fmt.Println("Fatal. Name is empty")
			c.Abort()
		}
		c.Set("name", input.Name)
		start := time.Now()
		// c.Next()
		runtime := time.Since(start).Seconds() * 1000

		c.JSON(200, gin.H{
			"status":  "success",
			"runtime": runtime,
		})
	}
}
