package main

import (
	"fmt"
	"gin-playground/middleware-gin/middleware"
	"time"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r.Use(middleware.ExampleMiddleware())

	userRoute := r.Group("/user")
	{
		userRoute.POST("/test", exampleFunc)
	}

	adminGroup := r.Group("/admin")
	adminGroup.Use(adminMiddleware())
	{
		adminGroup.POST("/test", exampleFunc)
	}

	r.Run()
}

func adminMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		fmt.Println("muda")
	}
}

func exampleFunc(c *gin.Context) {
	name, exist := c.Get("name")
	if !exist {
		c.JSON(500, gin.H{
			"message": "cannot get name",
		})
		c.Abort()
	}
	fmt.Println("Ex func: ", name)
	time.Sleep(888 * time.Millisecond)
	msg := fmt.Sprintf("hello there, %v", name)
	c.JSON(200, gin.H{
		"msg":  msg,
		"tips": "Notice the diff of commented / uncomment c.Next() line",
	})
}
